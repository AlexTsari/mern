# Goalsetter MERN app


This is a goal setter app, it is built for practicing the MERN stack and Redux. In this simple app you can register/login in your profile, set and remove goals. 🚀

### Technologies used

- JavaScript
- MongoDB
- Express.js
- React.js
- Node.js
- Redux

### Install dependencies

```
# Backend deps
npm install

# Frontend deps
cd frontend
npm install
```

### Run Server

```
npm run server
```

## Demo

https://main--fabulous-jelly-b77a9b.netlify.app/